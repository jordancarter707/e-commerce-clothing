import React from "react";
import './homePage.scss';

const HomePage = () => {
    return(
        <main className='homepage'>
            <div className='directory-menu'>
                <section className="menu-item">
                    <div className="content">
                        <h1 className="title"> Hats </h1>
                        <span className="sub-title"> SHOP NOW </span>
                    </div>
                </section>
                
                <section className="menu-item">
                    <div className="content">
                        <h1 className="title"> Jackets </h1>
                        <span className="sub-title"> SHOP NOW </span>
                    </div>
                </section>
                
                <section className="menu-item">
                    <div className="content">
                        <h1 className="title"> Sneakers </h1>
                        <span className="sub-title"> SHOP NOW </span>
                    </div>
                </section>
                
                <section className="menu-item">
                    <div className="content">
                        <h1 className="title"> Women's </h1>
                        <span className="sub-title"> SHOP NOW </span>
                    </div>
                </section>
                
                <section className="menu-item">
                    <div className="content">
                        <h1 className="title"> Men's </h1>
                        <span className="sub-title"> SHOP NOW </span>
                    </div>
                </section>
            </div>
        </main>
    );
}
export default HomePage;